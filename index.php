<?php
//Задание 1
$numbers = [2, 6, 12];
echo $numbers[1] . "<br>";
//Задание 2
$lines = ["sergey", "nesterovich", "victorovich"];
echo $lines[1] . "<br>";
//Задание 3
$one = ["34" => "8"];
echo $one[34] . "<br>";
//Задание 4
$dictionary = [
  "мышь" => "это животное грызун",
  "конь" => "скачет по полям",
  "хрюшка" => "кушает желуди"
];
//Задание 5
$dictionary  = [
  "mouse" => "это животное грызун",
  "horse" => "скачет по полям",
  "piggy" => "кушает желуди"
];
//Задание 6
print_r ($dictionary);
echo "<br>";
//Задание 7
echo $dictionary[mouse] . "<br>";
//Задание 8
echo $dictionary[piggy] . "<br>";
//Задание 9
echo $numbers[2] . "<br>";
//Задание 10
$numbers[2] = 3.14;
echo $numbers[2] . "<br>";
//Задание 11
$days = [
  "1" => "понедельник",
  "2" => "вторник",
  "3" => "среда",
  "4" => "четверг",
  "5" => "пятница",
  "6" => "суббота",
  "7" => "воскресенье"
];
echo $days[3];
